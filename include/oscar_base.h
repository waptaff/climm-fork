/* $Id$ */

#ifndef CLIMM_ICQV8_FLAP_H
#define CLIMM_ICQV8_FLAP_H

#define CLI_HELLO 1

#define FLAP_VER_MAJOR       5
#define FLAP_VER_MINOR      37
#define FLAP_VER_LESSER      1
#define FLAP_VER_BUILD    3828
#define FLAP_VER_SUBBUILD   85

/* test in this order */
#define STATUSF_ICQINV       0x00000100
#define STATUSF_ICQDND       0x00000002
#define STATUSF_ICQOCC       0x00000010
#define STATUSF_ICQNA        0x00000004
#define STATUSF_ICQAWAY      0x00000001
#define STATUSF_ICQFFC       0x00000020

#define STATUS_ICQOFFLINE    0xffffffff
#define STATUS_ICQINV         STATUSF_ICQINV
#define STATUS_ICQDND        (STATUSF_ICQDND | STATUSF_ICQOCC | STATUSF_ICQAWAY)
#define STATUS_ICQOCC        (STATUSF_ICQOCC | STATUSF_ICQAWAY)
#define STATUS_ICQNA         (STATUSF_ICQNA  | STATUSF_ICQAWAY)
#define STATUS_ICQAWAY        STATUSF_ICQAWAY
#define STATUS_ICQFFC         STATUSF_ICQFFC
#define STATUS_ICQONLINE     0x00000000


void FlapCliHello (Server *serv);
void FlapCliIdent (Server *serv);
void FlapCliCookie (Server *serv, const char *cookie, UWORD len);
void FlapCliGoodbye (Server *serv);
void FlapCliKeepalive (Server *serv);
void FlapChannel4 (Server *serv, Packet *pak);

void SrvCallBackFlap (Event *event);

Packet *FlapC (UBYTE channel);
void    FlapSend (Server *serv, Packet *pak);
void    FlapPrint (Packet *pak);

Packet *UtilIOReceiveTCP2 (Connection *conn);
void    UtilIOSendTCP2 (Connection *conn, Packet *pak);

Event  *OscarLogin (Server *serv);
#define OscarLogout(s) FlapCliGoodbye(s)

Server *SrvRegisterUIN (Server *serv, const char *pass);

status_t     IcqToStatus   (UDWORD status);
UDWORD       IcqFromStatus (status_t status);
statusflag_t IcqToFlags    (UDWORD status);
UDWORD       IcqFromFlags  (statusflag_t flags);
UDWORD       IcqIsUIN      (const char *screen);

#endif
