/* $Id$ */

#ifndef CLIMM_COLOR_H
#define CLIMM_COLOR_H

#define ESC "\x1b"
#define ESCC '\x1b'

#define BLACK           ESC "[0;30m"
#define RED             ESC "[0;31m"
#define GREEN           ESC "[0;32m"
#define YELLOW          ESC "[0;33m"
#define BLUE            ESC "[0;34m"
#define MAGENTA         ESC "[0;35m"
#define CYAN            ESC "[0;36m"
#define WHITE           ESC "[0;37m"
#define HIBLACK         ESC "[0;90m"
#define HIRED           ESC "[0;91m"
#define HIGREEN         ESC "[0;92m"
#define HIYELLOW        ESC "[0;93m"
#define HIBLUE          ESC "[0;94m"
#define HIMAGENTA       ESC "[0;95m"
#define HICYAN          ESC "[0;96m"
#define HIWHITE         ESC "[0;97m"
#define BGBLACK         ESC "[40m"
#define BGRED           ESC "[41m"
#define BGGREEN         ESC "[42m"
#define BGYELLOW        ESC "[43m"
#define BGBLUE          ESC "[44m"
#define BGMAGENTA       ESC "[45m"
#define BGCYAN          ESC "[46m"
#define BGWHITE         ESC "[47m"
#define HIBGBLACK       ESC "[100m"
#define HIBGRED         ESC "[101m"
#define HIBGGREEN       ESC "[102m"
#define HIBGYELLOW      ESC "[103m"
#define HIBGBLUE        ESC "[104m"
#define HIBGMAGENTA     ESC "[105m"
#define HIBGCYAN        ESC "[106m"
#define HIBGWHITE       ESC "[107m"
#define SGR0            ESC "[0m"
#define BOLD            ESC "[1m"
#define DIM             ESC "[2m"
#define UL              ESC "[4m"
#define INV             ESC "[7m"

#define COLNONE         ContactPrefStr (NULL, CO_COLORNONE)
#define COLSERVER       ContactPrefStr (NULL, CO_COLORSERVER)
#define COLCLIENT       ContactPrefStr (NULL, CO_COLORCLIENT)
#define COLINVCHAR      ContactPrefStr (NULL, CO_COLORINVCHAR)
#define COLERROR        ContactPrefStr (NULL, CO_COLORERROR)
#define COLDEBUG        ContactPrefStr (NULL, CO_COLORDEBUG)
#define COLQUOTE        ContactPrefStr (NULL, CO_COLORQUOTE)
#define COLMESSAGE      ContactPrefStr (cont, CO_COLORMESSAGE)
#define COLSENT         ContactPrefStr (cont, CO_COLORSENT)
#define COLACK          ContactPrefStr (cont, CO_COLORACK)
#define COLINCOMING     ContactPrefStr (cont, CO_COLORINCOMING)
#define COLCONTACT      ContactPrefStr (cont, CO_COLORCONTACT)

#define COLINDENT       ESC "v"
#define COLEXDENT       ESC "^"
#define COLMSGINDENT    ESC "<"
#define COLMSGEXDENT    ESC ">"
#define COLSINGLE       ESC "."

#endif
