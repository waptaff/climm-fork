/* $Id$ */

#ifndef CLIMM_PREFERENCES_H
#define CLIMM_PREFERENCES_H

#include <regex.h>
#include "util_opts.h"
#include "util_tcl.h"

typedef struct Msg_Highlight_Rules_s Msg_Highlight_Rules;

struct Msg_Highlight_Rules_s
{
    Msg_Highlight_Rules *next;        /* Next entry or NULL                       */
    char                *nick_regex;  /* Regex for nick matching                  */
    regex_t              _nick_regex; /* Compiled regex for nick matching         */
    char                *msg_regex;   /* Regex for highlighting matching          */
    regex_t              _msg_regex;  /* Compiled regex for highlighting matching */
    char                *highlight;   /* Highlighting code                        */
};

struct Preferences_s
{
    char                *locale;            /* used locale (encoding-free)     */
    char                *locale_orig;       /* original locale (as from ENV)   */
    char                *locale_full;       /* original locale (not C/POSIX)   */
    BOOL                 locale_broken;
    UBYTE                enc_loc;           /* the local character encoding    */

    Opt                  copts;             /* global (default) contact flags  */

    UDWORD               verbose;           /* verbosity to use on startup     */
    UWORD                sound;             /* how to beep                     */
    status_t             status;            /* status to use when logging in   */
    UWORD                screen;            /* manual max term width; 0 = auto */
    UDWORD               flags;             /* flags for output                */
    UDWORD               away_time;         /* auto-away timeout; 0 = disable  */

    char                *defaultbasedir;    /* the default base dir            */
    char                *basedir;           /* base dir for climmrc etc.       */
    char                *rcfile;            /* the preference file to load     */
    char                *statfile;          /* the status file to load         */
    char                *logplace;          /* where to log to                 */
    char                *logname;           /* which name to use for logging   */

    char                *event_cmd;         /* command to execute for events   */

    SBYTE                chat;
    SBYTE                autoupdate;

    char                *prompt_strftime;   /* user defined prompt             */
    char                *prompt;            /* user defined prompt             */
    char                *msg_regex_ignore;  /* hide messages matching regex    */
    regex_t              _msg_regex_ignore; /* compiled regex                  */
    Msg_Highlight_Rules *mhr;               /* Message highlighting rules      */

#ifdef ENABLE_TCL
    tcl_pref_p tclscript;
    tcl_callback *tclout;
#endif

    /* Much more stuff to go here - %TODO% */
};

Preferences *PreferencesC(void);
void         PreferencesInit(Preferences *pref);
BOOL         PrefLoad(Preferences *pref);
const char  *PrefSetColorScheme(UBYTE scheme);
BOOL         Msg_Highlight_Rules_append(Msg_Highlight_Rules **mhr, const char *msg_regex, const char *highlight, const char *nick_regex);

#define PrefDefUserDir(pref) (pref->defaultbasedir ? pref->defaultbasedir : PrefDefUserDirReal(pref))
#define PrefUserDir(pref)    (pref->basedir        ? pref->basedir        : PrefUserDirReal(pref)   )
#define PrefLogName(pref)    (pref->logname        ? pref->logname        : PrefLogNameReal(pref)   )

#define AUTOUPDATE_CURRENT 6

const char *PrefDefUserDirReal(Preferences *pref);
const char *PrefUserDirReal(Preferences *pref);
const char *PrefLogNameReal(Preferences *pref);
const char *PrefRealPath(const char *path);
#define s_realpath PrefRealPath

#define FLAG_DELBS          (1 <<  0)
#define FLAG_DEP_CONVRUSS   (1 <<  1)
#define FLAG_DEP_CONVEUC    (1 <<  2)
#define FLAG_FUNNY          (1 <<  3)
#define FLAG_COLOR          (1 <<  4)
#define FLAG_DEP_HERMIT     (1 <<  5)
#define FLAG_DEP_LOG        (1 <<  6)
#define FLAG_DEP_LOG_ONOFF  (1 <<  7)
#define FLAG_AUTOREPLY      (1 <<  8)
#define FLAG_UINPROMPT      (1 <<  9)
#define FLAG_LIBR_BR        (1 << 10) /* 0, 3: possible line break before message */
#define FLAG_LIBR_INT       (1 << 11) /* 2, 3: indent if appropriate              */
#define FLAG_DEP_QUIET      (1 << 12)
#define FLAG_DEP_ULTRAQUIET (1 << 13)
#define FLAG_AUTOSAVE       (1 << 14)
#define FLAG_AUTOFINGER     (1 << 15)
#define FLAG_USERPROMPT     (1 << 16)
/*      FLAG_S5
 *      FLAG_S5_USE
 */

#define SFLAG_BEEP  1
#define SFLAG_EVENT 2

#endif /* CLIMM_PREFERENCES_H */

