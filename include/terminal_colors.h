#ifndef CLIMM_TERMINAL_COLORS_H
#define CLIMM_TERMINAL_COLORS_H 1

#define TERMINAL_COLORS_ENTRY_COLORNAME_SIZE 16
#define TERMINAL_COLORS_ENTRY_ANSICODE_SIZE  128

extern struct terminal_colors_entry *terminal_colors_by_name;
extern struct terminal_colors_entry *terminal_colors_by_ansi;

struct terminal_colors_entry {
	char           colorname[TERMINAL_COLORS_ENTRY_COLORNAME_SIZE];
	char           ansicode[TERMINAL_COLORS_ENTRY_ANSICODE_SIZE];
	UT_hash_handle hh1; /* terminal_colors_by_name handle */
	UT_hash_handle hh2; /* terminal_colors_by_ansi handle */
};

int terminal_colors_find_by_name(const char *colorname, char *ansicode);
int terminal_colors_find_by_code(const char *ansicode,  char *colorname);
int terminal_colors_init();

#endif
