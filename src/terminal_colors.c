#include "climm.h"
#include "uthash.h"
#include "terminal_colors.h"

struct terminal_colors_entry *terminal_colors_by_name = NULL;
struct terminal_colors_entry *terminal_colors_by_ansi = NULL;

int terminal_colors_find_by_name(const char *colorname, char *ansicode) { /* {{{ */
	struct terminal_colors_entry *tctmp = NULL;
	HASH_FIND(hh1, terminal_colors_by_name, colorname, strlen(colorname), tctmp);
	if (tctmp == NULL) {
		ansicode = NULL;
		return FALSE;
	}
	else {
		strncpy(ansicode, tctmp->ansicode, TERMINAL_COLORS_ENTRY_ANSICODE_SIZE);
		return TRUE;
	};
}; /* }}} */
int terminal_colors_find_by_code(const char *ansicode, char *colorname) { /* {{{ */
	struct terminal_colors_entry *tctmp = NULL;
	HASH_FIND(hh2, terminal_colors_by_ansi, ansicode, strlen(ansicode), tctmp);
	if (tctmp == NULL) {
		colorname = NULL;
		return FALSE;
	}
	else {
		strncpy(colorname, tctmp->colorname, TERMINAL_COLORS_ENTRY_COLORNAME_SIZE);
		return TRUE;
	};
}; /* }}} */
int terminal_colors_init() { /* {{{ */
	int i;
	struct terminal_colors_entry *s;
	const char                   **n;
	const char                   *ccassoc[] = {
		"black",   "\x1b[0;30m",
		"red",     "\x1b[0;31m",
		"green",   "\x1b[0;32m",
		"yellow",  "\x1b[0;33m",
		"blue",    "\x1b[0;34m",
		"magenta", "\x1b[0;35m",
		"cyan",    "\x1b[0;36m",
		"white",   "\x1b[0;37m",
		NULL
	};
	/* Standard 16 colors */
	for (n = ccassoc; *n != NULL; n += 2) {
		s = (struct terminal_colors_entry*)calloc(1, sizeof(struct terminal_colors_entry));
		strcpy(s->colorname, *n      );
		strcpy(s->ansicode,  *(n + 1));
		HASH_ADD(hh1, terminal_colors_by_name, colorname, strlen(*n),       s);
		HASH_ADD(hh2, terminal_colors_by_ansi, ansicode,  strlen(*(n + 1)), s);
	};
	/* Foreground 256 colors */
	for (i = 0; i < 256; i++) {
		s = (struct terminal_colors_entry*)calloc(1, sizeof(struct terminal_colors_entry));
		snprintf(s->colorname, TERMINAL_COLORS_ENTRY_COLORNAME_SIZE, "color%d",       i);
		snprintf(s->ansicode,  TERMINAL_COLORS_ENTRY_ANSICODE_SIZE,  "\x1b[38;5;%dm", i);
		HASH_ADD(hh1, terminal_colors_by_name, colorname, strlen(s->colorname), s);
		HASH_ADD(hh2, terminal_colors_by_ansi, ansicode,  strlen(s->ansicode),  s);
	};
	/* Background 256 colors */
	for (i = 0; i < 256; i++) {
		s = (struct terminal_colors_entry*)calloc(1, sizeof(struct terminal_colors_entry));
		snprintf(s->colorname, TERMINAL_COLORS_ENTRY_COLORNAME_SIZE, "bgcolor%d",     i);
		snprintf(s->ansicode,  TERMINAL_COLORS_ENTRY_ANSICODE_SIZE,  "\x1b[48;5;%dm", i);
		HASH_ADD(hh1, terminal_colors_by_name, colorname, strlen(s->colorname), s);
		HASH_ADD(hh2, terminal_colors_by_ansi, ansicode,  strlen(s->ansicode),  s);
	};

	return TRUE;
}; /* }}} */

