climm-fork is a fork of the stagnant climm (previously micq) codebase.

See some historical information about the project:
https://en.wikipedia.org/wiki/Climm

# What is climm?

climm is a terminal-based command-line instant messaging application, that
supports both ICQ8 and XMPP protocols.

Since ICQ has moved on to ICQ10 protocol, this application is de facto only
supporting XMPP.

A picture is worth a thousand words:

![climm screenshot](doc/climm_shot.png)

# Summary of the major changes since the fork happened

- 256-color mode is now supported and may be used to color the interface.
- Compatibility with recent versions of GnuTLS, jabberd2, openssl, TCL.
- Configurable hiding of messages matching a regular expression.
- Crude support for group chats.
- Full Unicode support in input, not just what's in UCS2.

Note that support for ICQ protocols versions above 8 is not planned (though
patches are welcome), and focus from current main developer is entirely towards
XMPP support.

As XMPP supports gateways, it is possible in theory to install a ICQ gateway
that supports the newest ICQ protocols on a XMPP server and to use climm-fork
to interact with it.


# Compilation / Installation

climm-fork uses GNU autotools, so installation should be straightforward:

```shell
autoreconf --install
./configure
make
make install
```

`autoreconf` is found inside the `autoconf` package under Debian and RedHat;
inside the `sys-devel/autoconf-wrapper` package under Gentoo.

Note that some development packages will be required for compilation.  Watch
for errors in the previous set of commands to get hints about which packages to
install.


# Is there more documentation?

Sure.  Have a look in the `doc/` directory.

